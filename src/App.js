import logo from './logo.svg';
import './App.css';
import Ex_ShoeShop_Hook from './Ex_ShoeShop_Hook/Ex_ShoeShop_Hook';

function App() {
  return (
    <div className="App">
    <Ex_ShoeShop_Hook/>
    </div>
  );
}

export default App;
