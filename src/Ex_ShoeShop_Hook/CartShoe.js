import React from 'react'

export default function CartShoe(props) {
  let {cart,DeleteItem,changeQuanity } = props;
  console.log(cart);
  let rednerTbody = ()=>{
    return cart.map((item)=>{
      return  <tr>
        <td>{item.id}</td>
        <td>{item.name}</td>
        <td>{item.price * item.soLuong}$</td>
        <td>
          <button className='btn btn-danger' onClick={()=>{
            changeQuanity(item.id,-1)
          }}>-</button>
          <strong>{item.soLuong}</strong>
          <button className='btn btn-success'
           onClick={()=>{
            changeQuanity(item.id,1)
          }}
          >+</button>
        </td>
        <td>
          <img src={item.image} style={{width:80}} alt="" />
        </td>
        <td>
          <button className='btn btn-danger my-3' onClick={()=>{
            DeleteItem(item.id) 
          }} >  Delete </button>
        </td>
      </tr>
    })
  }
  return (
    <div>
 <div className="modal fade" id="exampleModalCenter" tabIndex={-1} role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div className="modal-dialog modal-dialog-centered" role="document">
    <div className="modal-content">
      <div className="modal-header">
        <h5 className="modal-title" id="exampleModalLongTitle">ShoeShop</h5>
        <button type="button" className="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div className="modal-body">
  <table className="table">
  <thead>
        <th>ID</th>
        <th>Name</th>
        <th>Price</th>
        <th>Quanity</th>
        <th>Image</th>
        <th>Action</th>
       </thead>
       <tbody>{rednerTbody()}</tbody>
  </table>
      </div>
      <div className="modal-footer">
        <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" className="btn btn-primary">Thanh Toán</button>
      </div>
    </div>
  </div>
</div>
 </div>
  )
}
