
import ItemShoe from './ItemShoe';


export default function ListShoe(props) {
let {listShoe,handleAddToCart} = props
let renderListShoe = ()=>{
  return listShoe.map((item)=>{
    return <ItemShoe shoe={item} handleAddToCart={handleAddToCart}/>
  }) 
}


  return (
    <div className='row'>
    {renderListShoe()}
    </div>
  )
}
