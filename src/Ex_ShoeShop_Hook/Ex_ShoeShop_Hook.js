
import CartShoe from './CartShoe';
import { dataShoe } from './DataShoe';
import ListShoe from './ListShoe'
import React, { useEffect, useState } from 'react'
export default function Ex_ShoeShop_Hook() {


  let data = dataShoe;
  let [cart,setCart] = useState([])
 
  useEffect(()=>{
  console.log("hi");
  },[cart])
  
  let handleAddToCart = (shoe)=>{
    let cloneCart = [...cart];
   let index = cart.findIndex((item)=>{
    return item.id == shoe.id
   })
   if (index == -1){
    let newShoe = {...shoe,soLuong:1}
    cloneCart.push(newShoe)
   }else{
    cloneCart[index].soLuong++;
   }
   setCart(cloneCart)
  }

  let DeleteItem = (id)=>{
let cloneCart = cart.filter((item)=>{
  return item.id != id
}) 
setCart(cloneCart)
  }

  let changeQuanity = (id,num)=>{
  let cloneCart = [...cart];
  let index = cart.findIndex((item)=>{
    return item.id == id;
  })
  cloneCart[index].soLuong+=num
  if(cloneCart[index].soLuong == 0){
    cloneCart.splice(index,1)
  }
  setCart(cloneCart)
  }


  return (
    <div className='container'>
      <h2  className='text-secondary' >ShoeShop</h2>
      <ListShoe listShoe={data}  handleAddToCart={handleAddToCart}/>
      <CartShoe  cart={cart} DeleteItem= {DeleteItem }  changeQuanity={changeQuanity}/>
      <a href=""  data-toggle="modal" data-target="#exampleModalCenter">
      <i class="fa fa-shopping-cart"></i>
      </a>
      </div>
  )
}
