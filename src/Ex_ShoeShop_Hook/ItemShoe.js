import React from 'react'
import { Card } from 'antd';
import { Button } from 'antd/es/radio';
const { Meta } = Card;
export default function ItemShoe(props) {
  let {shoe,handleAddToCart} = props;
  return (
    <div className='col-3 my-2'>
<Card
    hoverable
    style={{
      width: 240,
    }}
    cover={<img alt="example" src={shoe.image} />}
  >
    <Meta title={shoe.name} description={shoe.price +"$"} />
<button className='btn btn-success my-3'  onClick={()=>{
handleAddToCart(shoe)
}} >Add To Cart</button>
  </Card>
    </div> 
  )
}
